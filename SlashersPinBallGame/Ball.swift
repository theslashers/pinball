//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit


class Ball:SKSpriteNode{
    
    func setUpBall(){
        self.physicsBody?.categoryBitMask = category.ballGroup.rawValue
        self.physicsBody?.collisionBitMask = category.flipperGroup.rawValue | category.bottomGroup.rawValue | category.plungerGroup.rawValue | category.bumperGroup.rawValue | category.wallGroup.rawValue | category.holderGroup.rawValue | category.ballStoperGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.flipperGroup.rawValue | category.bottomGroup.rawValue | category.plungerGroup.rawValue | category.bumperGroup.rawValue | category.exitGroup.rawValue | category.entranceGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        
    }
}