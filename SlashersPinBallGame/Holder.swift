//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Holder:SKSpriteNode{
    
        func setUpHolder(){
            self.physicsBody?.categoryBitMask = category.holderGroup.rawValue
            self.physicsBody?.collisionBitMask = category.ballGroup.rawValue
            self.physicsBody?.contactTestBitMask = 0
            self.physicsBody?.usesPreciseCollisionDetection = true
        }
}
