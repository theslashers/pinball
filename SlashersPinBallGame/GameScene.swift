//
//  GameScene.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright (c) 2016 BunnyPhantom. All rights reserved.
//

import SpriteKit
import MapKit
import CoreLocation
import CoreMotion
enum category:UInt32{
    case flipperGroup = 1
    case ballGroup = 2
    case bottomGroup = 4
    case bumperGroup = 8
    case flipperStopGroup = 16
    case plungerGroup = 32
    case switchGroup = 64
    case wallGroup = 128
    case railingGroup = 256
    case entranceGroup = 512
    case exitGroup = 1024
    case ballAboveGroup = 2048
    case holderGroup = 4096
    case ballStoperGroup = 8192
}

class GameScene: SKScene, SKPhysicsContactDelegate{
    
    
    var scenceWidth:CGFloat = 0
    var scenceHeight:CGFloat = 0
    var mainBall:Ball = Ball()
    var myPlunger:Plunger = Plunger()
    var leftFlipper:Flipper = Flipper()
    var rightFlipper:Flipper = Flipper()
    var bottomOfDeath:SKNode = SKNode()
    var ballActive:Bool = false
    
    var currentScore:Int32 = 0
    var scoreLabel:SKLabelNode = SKLabelNode()
    var X2Swithces = [mySwitches]()
    var X2MuliplierActive = false
    var multipler = 1
    var lives = 0
    var startX:CGFloat = 0
    var startY:CGFloat = 0
    var livesArray = [SKSpriteNode]()
    var myBallStoper:BallStoper = BallStoper()
    //capture motion
    var movementManager = CMMotionManager()
    
    let bumperTexture1: SKTexture = SKTexture(imageNamed: "CircleAnimation1")
    let bumperTexture2: SKTexture = SKTexture(imageNamed: "CircleAnimation2")
    let bumperTexture3: SKTexture = SKTexture(imageNamed: "CircleAnimation3")
    let bumperTexture4: SKTexture = SKTexture(imageNamed: "CircleAnimation4")
    let bumperTexture5: SKTexture = SKTexture(imageNamed: "CircleAnimation5")
    
    let leftSideTexture1: SKTexture = SKTexture(imageNamed: "LeftBumper1")
    let leftSideTexture2: SKTexture = SKTexture(imageNamed: "LeftBumper2")
    let leftSideTexture3: SKTexture = SKTexture(imageNamed: "LeftBumper3")
    
    let rightSideTexture1: SKTexture = SKTexture(imageNamed: "RightBumper1")
    let rightSideTexture2: SKTexture = SKTexture(imageNamed: "RightBumper2")
    let rightSideTexture3: SKTexture = SKTexture(imageNamed: "RightBumper3")
    
    var deathX:CGFloat = 0
    var deathY:CGFloat = 0
    var death:SKSpriteNode = SKSpriteNode()
    override func didMoveToView(view: SKView) {
        
        
        //set up boarder
        let border = SKPhysicsBody(edgeLoopFromRect: self.frame)
        border.friction = 0
        self.physicsBody = border
        physicsWorld.contactDelegate = self
        let bottomOfScreen = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 1)
        bottomOfDeath.physicsBody = SKPhysicsBody(edgeLoopFromRect: bottomOfScreen)
        addChild(bottomOfDeath)
        bottomOfDeath.physicsBody!.categoryBitMask = category.bottomGroup.rawValue
        death = childNodeWithName("death") as! SKSpriteNode
        death.physicsBody = SKPhysicsBody(rectangleOfSize: death.size)
        death.physicsBody!.categoryBitMask = category.bottomGroup.rawValue
        deathX = death.position.x
        deathY = death.position.y
        death.physicsBody?.collisionBitMask = category.ballGroup.rawValue
        death.physicsBody?.contactTestBitMask = category.ballGroup.rawValue
        death.physicsBody?.affectedByGravity = false
        death.physicsBody?.usesPreciseCollisionDetection = true
        /* Setup your scene here */
        scenceWidth = self.frame.width
        scenceHeight = self.frame.height
        
        setUpChildrenNodes()
        
        //set up motion proproites
        movementManager.accelerometerUpdateInterval = 0.2
        movementManager.gyroUpdateInterval = 0.2
        movementManager.startDeviceMotionUpdates()
        movementManager.startAccelerometerUpdatesToQueue(NSOperationQueue.currentQueue()!) { (accelerometerData: CMAccelerometerData?, NSError) -> Void in
            
            self.outputAccData(accelerometerData!.acceleration)
            if(NSError != nil) {
                print("\(NSError)")
            }
        }
        
        
    }
    func outputAccData(acceleration: CMAcceleration){
        //print(movementManager.deviceMotion?.attitude.roll)
        //print(movementManager.deviceMotion?.attitude.yaw)
        
        let xDirection = acceleration.x * 10
        let yDirection = -3.0
        if(xDirection < -4.0 || xDirection > 4.0){
            tilt()
        }
        physicsWorld.gravity = CGVector(dx: xDirection, dy: yDirection)
        
    }
    //
    func tilt(){
        
        if ballActive{
            //ballActive = false
            
            mainBall.removeFromParent()
            if(lives < 0){
                createNewBall()
            }else{
                lives += 1
            }
        }
        
        
    }
    //create a ball and remove live image
    func createNewBall(){
        livesArray[lives].texture = SKTexture(imageNamed: "piece1-2")
        lives += 1
        let newBall:Ball = Ball(texture: SKTexture(imageNamed: "piece2-2"))
        newBall.position.x = startX
        newBall.position.y = startY
        newBall.physicsBody = SKPhysicsBody(rectangleOfSize: newBall.size)
        newBall.physicsBody?.dynamic = true
        newBall.physicsBody?.affectedByGravity = true
        newBall.physicsBody?.allowsRotation = true
        newBall.physicsBody?.pinned = false
        newBall.size.height = CGFloat(15)
        newBall.size.width = CGFloat(15)
        newBall.setUpBall()
        addChild(newBall)
        mainBall = newBall
        
    }
    func setUpChildrenNodes(){
        scoreLabel = childNodeWithName("Score") as! SKLabelNode
        leftFlipper = childNodeWithName("leftFlipper") as! Flipper
        rightFlipper = childNodeWithName("rightFlipper") as! Flipper
        myPlunger = childNodeWithName("plunger") as! Plunger
        mainBall = childNodeWithName("MainBall") as! Ball
        startX = mainBall.position.x
        startY = mainBall.position.x
        
        let stoper = childNodeWithName("FlipperStoper") as! FlipperStoper
        stoper.setUpStoper()
        leftFlipper.setUpFlipper()
        rightFlipper.setUpFlipper()
        myPlunger.setUpPlunger()
        for index in 0...2{
            let life = SKSpriteNode(imageNamed: "piece2-2")
            
            life.position = CGPointMake(UIScreen.mainScreen().bounds.width - life.size.width/2 - CGFloat(index) * life.size.width, UIScreen.mainScreen().bounds.height - life.size.height/2)
            addChild(life)
            livesArray.append(life)
        }
        self.enumerateChildNodesWithName("//holder_[0-9]*") { (node, stop) -> Void in
            
            if let holder:Holder = node as? Holder{
                holder.setUpHolder()
                
            }
            
            
        }
        
        self.enumerateChildNodesWithName("//bumper_[0-9]*") { (node, stop) -> Void in
            
            if let myBumper:Bumper = node as? Bumper{
                myBumper.setUpBumper()
            }
            
            
        }
        var sideBumper = childNodeWithName("leftBumper") as! Bumper
        sideBumper.setUpBumper()
        sideBumper = childNodeWithName("rightBumper") as! Bumper
        sideBumper.setUpBumper()
        self.enumerateChildNodesWithName("//switchesx2_[0-9]*") { (node, stop) -> Void in
            if let mySwitch:mySwitches = node as? mySwitches{
                mySwitch.setUpSwitch("group1")
                self.X2Swithces.append(mySwitch)
            }
        }
        self.enumerateChildNodesWithName("//rail_[0-9]*") { (node, stop) -> Void in
            
            if let rail:Rail = node as? Rail{
                rail.setUpRail()
                
                
            }
            
        }
        self.enumerateChildNodesWithName("//exit_[0-9]*") { (node, stop) -> Void in
            
            if let exit:Exit = node as? Exit{
                exit.setUpExit()
            }
            
        }
        self.enumerateChildNodesWithName("//entrance_[0-9]*") { (node, stop) -> Void in
            
            if let entrance:Entrance = node as? Entrance{
                entrance.setUpEntrance()
            }
            
        }
        self.enumerateChildNodesWithName("//wall_[0-9]*") { (node, stop) -> Void in
            
            if let wall:Wall = node as? Wall{
                wall.setUpWall()
            }
            
        }
        
        mainBall.setUpBall()
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if(!ballActive){
            myPlunger.adjustHeight()
            
        }else{
            //if tapps anyware on right side of the screen then right bumber is called else left
            for touch in touches{
                let location = touch.locationInNode(self)
                if(location.x > scenceWidth/2){
                    pressRightFlipper()
                }else{
                    pressLeftFlipper()
                }
            }
        }
        
        
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if(!ballActive){
            myPlunger.adjustHeight()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if(!ballActive){
            
            myPlunger.releasePlunger()
            
        }else{
            for touch in touches{
                let location = touch.locationInNode(self)
                if(location.x > scenceWidth/2){
                    endRightFlipper()
                }else{
                    endLeftFlipper()
                }
            }
        }
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var objectOne: SKPhysicsBody
        var objectTwo: SKPhysicsBody
        
        // lower category is always stored in object one
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            objectOne = contact.bodyA
            objectTwo = contact.bodyB
        } else {
            objectOne = contact.bodyB
            objectTwo = contact.bodyA
        }
        
        if ((objectOne.categoryBitMask == category.flipperGroup.rawValue && objectTwo.categoryBitMask == category.flipperStopGroup.rawValue)){
            if let flipper:Flipper = objectOne.node as? Flipper{
                flipper.lockFlipperUp()
            }
        }
        if ((objectOne.categoryBitMask == category.ballGroup.rawValue && objectTwo.categoryBitMask == category.bottomGroup.rawValue)){
            if ballActive{
                objectOne.node?.removeFromParent()
                //ballActive = false
                
                if(lives < 0){
                    createNewBall()
                }else{
                    lives += 1
                }
            }
            
            //self.mainBall.position = CGPointMake(500, 500)
        }
        if ((objectOne.categoryBitMask == category.ballGroup.rawValue && objectTwo.categoryBitMask == category.bumperGroup.rawValue)){
            currentScore += 10 * multipler
            //create array of movements for left
            if(objectTwo.node!.name == "leftBumper"){
                let bumperAnimation = SKAction.repeatAction(SKAction.animateWithTextures([leftSideTexture1, leftSideTexture2, leftSideTexture3, leftSideTexture2, leftSideTexture1], timePerFrame: 0.08), count: 3)
                objectTwo.node!.runAction(bumperAnimation)
            }else if(objectTwo.node!.name == "rightBumper"){
                let bumperAnimation = SKAction.repeatAction(SKAction.animateWithTextures([rightSideTexture1, rightSideTexture2, rightSideTexture3, rightSideTexture2, rightSideTexture1], timePerFrame: 0.08), count: 3)
                objectTwo.node!.runAction(bumperAnimation)
            }else{
                let bumperAnimation = SKAction.repeatAction(SKAction.animateWithTextures([bumperTexture1, bumperTexture2, bumperTexture3, bumperTexture2, bumperTexture1], timePerFrame: 0.08), count: 3)
                objectTwo.node!.runAction(bumperAnimation)
            }
            
            
            
        }
        if ((objectOne.categoryBitMask == category.ballGroup.rawValue && objectTwo.categoryBitMask == category.switchGroup.rawValue)){
            if let mySwitch:mySwitches = objectTwo.node as? mySwitches{
                mySwitch.setActiveBool(true)
                mySwitch.texture = SKTexture(imageNamed: "Switch1")
            }
            
        }
        if ((objectOne.categoryBitMask == category.ballGroup.rawValue && objectTwo.categoryBitMask == category.entranceGroup.rawValue)){
            if(objectTwo.node?.name == "entrance_1"){
                //ballActive = false
            }else{
                raiseBall()
            }
            
            
        }
        if ((objectOne.categoryBitMask == category.ballGroup.rawValue && objectTwo.categoryBitMask == category.exitGroup.rawValue)){
            if(objectTwo.node?.name == "exit_1"){
                ballActive = true
                //print("boom")
            }else{
                lowerBall()
            }
            
        }
        if ((objectOne.categoryBitMask == category.ballGroup.rawValue && objectTwo.categoryBitMask == category.plungerGroup.rawValue)){
            //myPlunger.plungerLock()
            
            
        }
        
        
        
        
    }
    
    func raiseBall(){
        
        mainBall.physicsBody?.collisionBitMask = category.flipperGroup.rawValue | category.bottomGroup.rawValue | category.plungerGroup.rawValue | category.bumperGroup.rawValue | category.railingGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.flipperGroup.rawValue | category.bottomGroup.rawValue | category.plungerGroup.rawValue | category.bumperGroup.rawValue | category.switchGroup.rawValue | category.exitGroup.rawValue | category.entranceGroup.rawValue
        mainBall.zRotation = CGFloat(9)
        
    }
    func lowerBall(){
        mainBall.physicsBody?.collisionBitMask = category.flipperGroup.rawValue | category.bottomGroup.rawValue | category.plungerGroup.rawValue | category.bumperGroup.rawValue | category.wallGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.flipperGroup.rawValue | category.bottomGroup.rawValue | category.plungerGroup.rawValue | category.bumperGroup.rawValue | category.exitGroup.rawValue | category.entranceGroup.rawValue
        mainBall.zRotation = CGFloat(2)
    }
    func randomNegate(value:CGFloat)->CGFloat{
        let negate = CGFloat(arc4random_uniform(2))
        
        var result = value
        if(negate == 1){
            result = -value
            
        }
        return result
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        leftFlipper.update()
        rightFlipper.update()
        myPlunger.update()
        scoreLabel.text = "Score:\(currentScore)"
        checkMultiplers()
        if(!ballActive){
            removeMultipliers()
            
        }
        if(lives >= 1){
            gameOver()
        }
        death.position.x = deathX
        death.position.y = deathY
        
    }
    func gameOver(){
        if let scene = GameOverScene(fileNamed:"GameOverScene") {
            // Configure the view.
            
            if let skView = view{
                skView.showsFPS = false
                skView.showsNodeCount = false
                
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .AspectFill
                
                skView.presentScene(scene)
            }
        }

    }
    func checkMultiplers(){
        var check = true
        if(!X2MuliplierActive){
            for mySwitch in X2Swithces{
                if(!mySwitch.getActiveBool()){
                    check = false
                    
                }
            }
            if(check){
                multipler = multipler * 2
                X2Swithces[0].startTimer()
                X2MuliplierActive = true
            }
            
        }else if !X2Swithces[0].getActiveBool(){
            X2MuliplierActive = false
            multipler = multipler / 2
            
        }
    }
    func removeMultipliers(){
        for mySwitch in X2Swithces{
            mySwitch.setActiveBool(false)
            mySwitch.texture = SKTexture(imageNamed: "Switch2")
        }
    }
    //pressing button
    func pressLeftFlipper(){
        leftFlipper.flipUP()
    }
    func pressRightFlipper(){
        
        rightFlipper.flipUP()
        
    }
    
    
    //letting button go
    func endLeftFlipper(){
        leftFlipper.flipDown()
    }
    func endRightFlipper(){
        
        rightFlipper.flipDown()
        
        
    }
}
