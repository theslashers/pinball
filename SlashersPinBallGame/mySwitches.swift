//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class mySwitches:SKSpriteNode{
        //take in value to determine which kind of switch it is
        //maybe make this an interface
    var groupName:String = ""
    var timer = NSTimer()
    //10 seconds for multipler
    var maxTime = 10
    var currentTime:UInt32 = 0
    var active:Bool = false
    func setUpSwitch(group:String){
        self.physicsBody?.categoryBitMask = category.switchGroup.rawValue
        self.physicsBody?.collisionBitMask = 0
        self.physicsBody?.contactTestBitMask = 0
        self.physicsBody?.usesPreciseCollisionDetection = true
        groupName = group
        
    }
    func getGroup()->String{
        return groupName
    }
    func getActiveBool()->Bool{
        return active
    }
    func setActiveBool(value:Bool){
        active = value
    }
    func startTimer(){
        currentTime = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(mySwitches.timerUp), userInfo: nil, repeats: true)
    }
    func timerUp(){
        currentTime += 1
        if(currentTime > 10){
            endTimer()
        }
    }
    func getTimer()->NSTimer{
        return timer
    }
    func endTimer(){
        
        timer.invalidate()
        active = false
    }
    func getTime()->UInt32{
        return currentTime
    }
}

