//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Flipper:SKSpriteNode{
    
    var startLocation:CGPoint = CGPointZero
    var rotateAngleUp:CGFloat = 0
    var rotateAngleDown:CGFloat = 0
    var isUp:Bool = false
    func setUpFlipper(){
        startLocation = self.position
        self.physicsBody?.categoryBitMask = category.flipperGroup.rawValue
        self.physicsBody?.collisionBitMask = category.ballGroup.rawValue | category.flipperStopGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.ballGroup.rawValue | category.flipperStopGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        //set up the rotation angle for the flippers
        if(self.name == "rightFlipper"){
            rotateAngleUp = -30
            rotateAngleDown = 30
            self.zRotation = DegreesToRadian(rotateAngleDown)
        }
        else if(self.name == "leftFlipper"){
            rotateAngleUp = 30
            rotateAngleDown = -30
            self.zRotation = DegreesToRadian(rotateAngleDown)
        }
        
    }
    //for some reason it is in radians
    //even though in the skscne it was degrees
    //no idea why it does this
    func radianToDegrees(radian: CGFloat)->CGFloat{
        var result:CGFloat
        result = (radian * 180) / CGFloat(M_PI)
        return result
    }
    func DegreesToRadian(degree: CGFloat)->CGFloat{
        var result:CGFloat
        result = (degree * CGFloat(M_PI)) / 180
        return result
    }

    
    func flipUP(){
        if(!isUp){
           isUp = true
            self.physicsBody?.dynamic = true
            self.physicsBody?.allowsRotation = true
            //add x based on current location
            self.physicsBody?.applyImpulse(CGVectorMake(0, 500))
        }
    }
    func flipDown(){
        isUp = false
        self.physicsBody?.dynamic = true
        self.physicsBody?.allowsRotation = true
        //should remove impulse
        self.physicsBody?.velocity = CGVectorMake(0, 0)
        self.physicsBody?.angularVelocity = 0
        
        let flip:SKAction = SKAction.rotateToAngle(DegreesToRadian(rotateAngleDown), duration: 0.1)
        let run:SKAction = SKAction.runBlock { 
            self.lockFlipperDown()
        }
        self.runAction(SKAction.sequence([flip,run]), withKey: "rotateDownAndLock")
    }
    
    func lockFlipperUp(){
        self.zRotation = DegreesToRadian(rotateAngleUp)
        lockFlipper()
    }
    func lockFlipperDown(){
        self.zRotation = DegreesToRadian(rotateAngleDown)
        lockFlipper()
    }
    func lockFlipper(){
        self.physicsBody?.dynamic = false
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.velocity = CGVectorMake(0, 0)
        self.physicsBody?.angularVelocity = 0
        self.physicsBody?.applyImpulse(CGVectorMake(0, 0))
    }
    //even though i pinned the node and never tell it to move the flipper still moved so this forces it to stay still
    func update(){
        self.position = startLocation
        //check if the rotation degree exceeds the limit fix it
    }
}
