//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class FlipperStoper:SKSpriteNode{
    
    func setUpStoper(){
        self.physicsBody?.categoryBitMask = category.flipperStopGroup.rawValue
        self.physicsBody?.collisionBitMask = category.flipperGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.flipperGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        
    }
}
