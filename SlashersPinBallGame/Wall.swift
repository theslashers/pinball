//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Wall:SKSpriteNode{
    
    func setUpWall(){
        self.physicsBody?.categoryBitMask = category.wallGroup.rawValue
        self.physicsBody?.collisionBitMask = category.ballGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.ballGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        
    }
}
