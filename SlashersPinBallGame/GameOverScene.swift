//
//  GameOverScene.swift
//  teamMario
//
//  Created by Shashankjayate Dhanakshirur on 2/22/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import UIKit
import SpriteKit

class GameOverScene: SKScene {
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let scene = GameScene(fileNamed:"GameScene") {
            
            if let location = touches.first?.locationInNode(self) {
                let touchedNode = nodeAtPoint(location)
                
                if touchedNode.name == "replay" {
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    // Configure the view.
                    let skView = self.view
                    skView!.showsFPS = false
                    skView!.showsNodeCount = false
                    /* Sprite Kit applies additional optimizations to improve rendering performance */
                    skView!.ignoresSiblingOrder = true
                    
                    /* Set the scale mode to scale to fit the window */
                    scene.scaleMode = .AspectFill
                    
                    skView!.presentScene(scene, transition: transition)
                }
                
            }
        }
    }

}
