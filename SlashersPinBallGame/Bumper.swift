//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit


class Bumper:SKSpriteNode{
    
    func setUpBumper(){
        self.physicsBody?.categoryBitMask = category.bumperGroup.rawValue
        self.physicsBody?.collisionBitMask = category.ballGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.ballGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        self.physicsBody?.restitution = 1.5
        //increase resituion to 2
        //radius divder
        //saftey bumper to give extra chance
    }
}
