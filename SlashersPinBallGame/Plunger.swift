//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Plunger:SKSpriteNode{
   
    let maxHeight:CGFloat = 300
    let minHeight:CGFloat = 200
    var impulse:CGFloat = 0
    var myScale:CGFloat = 0
    var startLocation:CGPoint = CGPointZero
    func setUpPlunger(){
        
        self.physicsBody?.categoryBitMask = category.plungerGroup.rawValue
        self.physicsBody?.collisionBitMask = category.ballGroup.rawValue
        self.physicsBody?.contactTestBitMask = category.ballGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        self.size.height = maxHeight
        startLocation = self.position
        //self.physicsBody?.applyImpulse(CGVectorMake(0, 5000))
        myScale =  self.yScale
    }
    
    func adjustHeight(){
        //make sure the hight is above the min
        if(self.size.height > minHeight){
            self.yScale -= 0.05
            //as plunger gets lowered the impules that it will have increases
            impulse += 10000
            
        }
    }
    //plunger is released add the impulse
    func releasePlunger(){
       
        self.physicsBody?.applyImpulse(CGVectorMake(0, 10000))
        impulse = 0
        self.yScale = myScale
        
    }
    func plungerLock(){
        
        self.physicsBody?.velocity = CGVectorMake(0, 0)
        self.physicsBody?.angularVelocity = 0
        self.physicsBody?.applyImpulse(CGVectorMake(0, 0))
        self.yScale = myScale
        
    }
   
    //even though i pinned the node and never tell it to move the flipper still moved so this forces it to stay still
    func update(){
        self.position = startLocation
        
        self.zRotation = 0
        //check if the rotation degree exceeds the limit fix it
    }
}
