//
//  Flipper.swift
//  SlashersPinBallGame
//
//  Created by kevin travers on 4/18/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Entrance:SKSpriteNode{
    
    func setUpEntrance(){
        self.physicsBody?.categoryBitMask = category.entranceGroup.rawValue
        self.physicsBody?.collisionBitMask = 0
        self.physicsBody?.contactTestBitMask = category.ballGroup.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
}
